FactoryGirl.define do
  factory :trip do
    title "MyString"
    description "MyText"
    from_date "2016-04-24"
    to_date "2016-04-24"
  end
end
