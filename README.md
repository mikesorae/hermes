# About

Hermes(仮)のWebアプリケーション開発用リポジトリです。

# Requisite

- ruby 2.1 - 2.2
- node 4.2 - 4.4
- mysql 5.6

# Setup

## Railsアプリケーション

```
bundle install
bundle exec rake db:create
bundle exec rake db:migrate
```

## nodeアプリケーション

```
npm install
```

# Usage

## Railsアプリケーション

```
bundle exec rails server
```

http://localhost:3000/
でアクセス出来ます。

## Styleguide

```
gulp styleguide
```

http://localhost:3001/
でアクセス出来ます。
