class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.string :title
      t.text :description
      t.date :from_date
      t.date :to_date

      t.timestamps null: false
    end
  end
end
