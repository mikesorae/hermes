var path = require('path');

const JS_DIR = './app/assets/javascripts/';
const JS_LIB_DIR = './app/assets/javascripts/libs/';

var config = {
  entry: 'main.babel.js',
  output: {
    filename: 'main.js'
  },
  module: {
    loaders: [
      { 
        test: /\.babel.js$/,  
        loader: 'babel-loader',
        exclude: '/node_modules/',
        query: {
          presets: ['es2015', 'stage-0']
        }
      }
    ]
  },
  resolve: {
    root: [
      path.resolve(JS_DIR)
    ],
    moduleDirectories: ['node_modules'],
    extensions: ['', '.babel.js', '.js']
  }
}

module.exports = config
