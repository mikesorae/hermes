var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var plumber = require('gulp-plumber');
var styleguide = require('sc5-styleguide');
var webserver = require('gulp-webserver');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require("gulp-autoprefixer");
var notify = require('gulp-notify');

// sc5-styleguideの生成タスク
var styleguideBuild = () => {
  return gulp.src(['app/assets/stylesheets/**/*.scss'])
    .pipe(styleguide.generate({
      title: 'Styleguide Example',
      server: false,
      overviewPath: 'README.md'
    }))
    .pipe(gulp.dest('styleguide'));
};


// sc5-styleguideスタイル適用タスク
var styleguideApply = () => {
  return gulp.src(['app/assets/stylesheets/main.scss'])
    .pipe(sourcemaps.init())
    .pipe(plumber({
      errorHandler: notify.onError('<%= error.message %>')
    }))
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(sourcemaps.write('styleguide/maps'))
    .pipe(styleguide.applyStyles())
    .pipe(gulp.dest('styleguide'));
};

// sc5-styleguide用webサーバ起動タスク
var styleguideServe = () => {
  gulp.src('styleguide')
    .pipe(webserver({
      host: 'localhost',
      port: 3333
    }));
};

var styleguideWatch = () => {
  watch('app/assets/stylesheets/**/*.scss', () => { gulp.start(['styleguide:build']) });
};

gulp.task('styleguide:generate', styleguideBuild);
gulp.task('styleguide:applystyles', styleguideApply);
gulp.task('styleguide:serve', styleguideServe);
gulp.task('styleguide:watch', styleguideWatch);

gulp.task('styleguide:build', ['styleguide:generate', 'styleguide:applystyles']);

gulp.task('styleguide', ['styleguide:build', 'styleguide:watch', 'styleguide:serve']);

