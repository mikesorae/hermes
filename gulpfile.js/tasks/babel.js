var gulp = require('gulp');
var webpack = require('gulp-webpack');
var plumber = require('gulp-plumber');
var watch = require('gulp-watch');
var notify = require('gulp-notify');
var config = require ('../webpack.config.js');

console.log(config);

var babelBuild = () => {
  gulp.src(['app/assets/javascripts/main.babel.js'])
    .pipe(webpack(config))
    .pipe(gulp.dest('app/assets/javascripts/build/'))
};

var babelWatch = () => {
  gulp.watch(['app/assets/javascripts/**/*.babel.js'], ['babel:build'])
};

gulp.task('babel:build', babelBuild);
gulp.task('babel:watch', babelWatch);

gulp.task('babel', ['babel:build', 'babel:watch']);
