var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var plumber = require('gulp-plumber');
var webserver = require('gulp-webserver');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');

// scssファイルのコンパイル用タスク
var styleBuild = () => {
  gulp.src(['app/assets/stylesheets/main.scss'])
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('app/assets/stylesheets/build'));
};

var styleWatch = () => {
  watch('app/assets/stylesheets/**/*.scss', () => { gulp.start(['style:build']) });
};

gulp.task('style:build', styleBuild);
gulp.task('style:watch', styleWatch);
gulp.task('style', ['style:build', 'style:watch']);
