var gulp = require('gulp');

var styleguide = require('./tasks/styleguide.js');
var style = require('./tasks/style.js');
var babel = require('./tasks/babel.js');

gulp.task('default', ['style', 'babel']);
