import Vue from 'vue';
import request from 'superagent';

export default Vue.extend({
  el: ()=> { 
    return '.trip-card-list' 
  },
  data: ()=> {
    return { 
      trips: [
        { id: 1, name: 'test1'},
        { id: 2, name: 'test2'}
      ],
      newTrip: {
        name: 'default'
      }
    }
  },
  methods: {
    addTrip: function(event) {
      console.log(this.newTrip.name);

      request
        .post('/trips.json')
        .set('Accept', 'application/json')
        .send({ 'title': this.newTrip.name })
        .end((err, res) => {
          console.log(res.body);
        });
    }
  }
})
